const userService = {
  userList: [
    { id: 1, username: "Chattrawut", password: "phoolakorn"},
    { id: 2, username: "August", password: "author"}
  ],
  lastId: 3,
  addUser(user) {
    delete user.passwordConfirm
    user.id = this.lastId++;
    this.userList.push(user);
  },
  updateUser(user) {
    user.passwordConfirm = ''
    const index = this.userList.findIndex(item => item.id === user.id);
    this.userList.splice(index, 1, user);
  },
  deleteUser(user) {
    user.passwordConfirm = ''
    const index = this.userList.findIndex(item => item.id === user.id);
    this.userList.splice(index, 1);
  },
  getUsers () {
    return [...this.userList]
  }
}

export default userService